@extends('layouts.guest')

@section('title', "Kontak")

@section('content')
<div class="breadcumb-wrapper" style="position: relative; background-image: url('{{ asset('assetsuser/images/head_bg.jpg') }}'); background-size: cover; background-position: center; height: 100%;">
    <div class="overlay" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); z-index: 1;"></div>
    <div class="breadcumb-content" style="position: relative; z-index: 2; color: white;">
        <h1 class="breadcumb-title" style="color: white;">Kontak Kami</h1>
        <br>
        <ul class="breadcumb-menu" style="list-style: none; padding: 0; margin: 0;">
            <li style="display: inline; margin-right: 10px;"><a href="/" style="color: white; text-decoration: none;">Beranda</a></li>
            <li style="display: inline;">Kontak Kami</li>
        </ul>
    </div>
</div>
<div class="space">
    <div class="container">
        <div class="row gy-4">
            <div class="col-xl-4 col-md-6">
                <div class="contact-info">
                    <div class="contact-info_icon"><i class="fas fa-location-dot"></i></div>
                    <div class="media-body">
                        <h4 class="box-title">Alamat Kantor Kami</h4><span class="contact-info_text">Jl. Tukad Unda VIII No.8a, Panjer, Depasar Selatan, Kota Denpasar, Bali 80234<span>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="contact-info">
                    <div class="contact-info_icon"><i class="fas fa-phone"></i></div>
                    <div class="media-body">
                        <h4 class="box-title">Hubungi Kami Kapan Saja</h4><span class="contact-info_text"><a
                            href="https://wa.me/6282266489324" target="_blank">+62 822 6648 9324</a></span>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="contact-info">
                    <div class="contact-info_icon"><i class="fas fa-envelope"></i></div>
                    <div class="media-body">
                        <h4 class="box-title">Kirim Email</h4><span class="contact-info_text"><a
                                href="mailto:balisanitya116@gmail.com">balisanitya116@gmail.com</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-smoke space" data-bg-src="assetsuser/img/bg/contact_bg_1.png" id="contact-sec">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="title-area mb-35 text-xl-start text-center"><span class="sub-title">
                        <div class="icon-masking me-2"><span class="mask-icon"
                                data-mask-src="assetsuser/img/theme-img/title_shape_2.svg"></span> <img
                                src="assetsuser/img/theme-img/title_shape_2.svg" alt="shape"></div>Hubungi Kami!
                    </span>
                    <h2 class="sec-title">Ada Pertanyaan?</h2>
                </div>
                <form action="{{ route('contact.store') }}" method="POST"  enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-6"><input type="text" class="form-control" name="namaawalan" id="name"
                                placeholder="Nama Depan"> <i class="fal fa-user"></i></div>
                        <div class="form-group col-md-6"><input type="text" class="form-control" name="namaakhiran" id="name"
                                placeholder="Nama Belakang"> <i class="fal fa-user"></i></div>
                        <div class="form-group col-md-6"><input type="email" class="form-control" name="email"
                                id="email" placeholder="Alamat Email"> <i class="fal fa-envelope"></i></div>
                        <div class="form-group col-md-6"><input type="tel" class="form-control" name="nomorhp"
                                id="number" placeholder="Nomor Telepon"> <i class="fal fa-phone"></i></div>
                        <div class="form-group col-12"><textarea name="pesan" id="message" cols="30" rows="3"
                                class="form-control" placeholder="Pesan Anda"></textarea> <i
                                class="fal fa-comment"></i></div>
                        <div class="form-btn text-xl-start text-center col-12"><button class="th-btn">Kirim Pesan<i
                                    class="fa-regular fa-arrow-right ms-2"></i></button></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="map-sec"><iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.1544000094927!2d115.2310247745227!3d-8.676863219192843!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd24122a272b13d%3A0xdb86b3e808e57e23!2sYAYASAN%20BALI%20SANITYA%20SEJAHTERA!5e0!3m2!1sid!2sid!4v1719893835270!5m2!1sid!2sid"
        allowfullscreen="" loading="lazy"></iframe></div>
@endsection

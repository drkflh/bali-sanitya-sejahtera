@extends('layouts.guest')

@section('title', "Services")

@section('content')
<div class="breadcumb-wrapper" style="position: relative; background-image: url('{{ asset('assetsuser/images/head_bg.jpg') }}'); background-size: cover; background-position: center; height: 100%;">
    <div class="overlay" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); z-index: 1;"></div>
    <div class="breadcumb-content" style="position: relative; z-index: 2; color: white;">
        <h1 class="breadcumb-title" style="color: white;">Pelatihan Kami</h1>
        <br>
        <ul class="breadcumb-menu" style="list-style: none; padding: 0; margin: 0;">
            <li style="display: inline; margin-right: 10px;"><a href="/" style="color: white; text-decoration: none;">Beranda</a></li>
            <li style="display: inline;">Pelatihan Kami</li>
        </ul>
    </div>
</div>

<section class="space" id="service-sec">
    <div class="container">
        <div class="row row-cols-md-2 gy-4">
            <div class="col">
                <div class="service-card">
                    <div class="service-card_number">01</div>
                    <div class="shape-icon">
                        <img src="{{asset('assetsuser/images/flag/jepang.png')}}" style="width: 60px" alt="Ikon">
                        <span class="dots"></span>
                    </div>
                    <h3 class="box-title">
                        <a href="/training/japanese-language-training">Pelatihan Bahasa Jepang</a>
                    </h3>
                    <div class="bg-shape">
                        <img src="assetsuser/img/bg/service_card_bg.png" alt="bg">
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="service-card">
                    <div class="service-card_number">02</div>
                    <div class="shape-icon">
                        <img src="{{asset('assetsuser/images/flag/korea.png')}}" style="width: 60px" alt="Ikon">
                        <span class="dots"></span>
                    </div>
                    <h3 class="box-title">
                        <a href="/training/korea-language-training">Pelatihan Bahasa Korea</a>
                    </h3>
                    <div class="bg-shape">
                        <img src="assetsuser/img/bg/service_card_bg.png" alt="bg">
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="service-card">
                    <div class="service-card_number">03</div>
                    <div class="shape-icon">
                        <img src="{{asset('assetsuser/images/flag/inggris.png')}}" style="width: 60px" alt="Ikon">
                        <span class="dots"></span>
                    </div>
                    <h3 class="box-title">
                        <a href="/training/inggris-language-training">Pelatihan Bahasa Inggris</a>
                    </h3>
                    <div class="bg-shape">
                        <img src="assetsuser/img/bg/service_card_bg.png" alt="bg">
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="service-card">
                    <div class="service-card_number">04</div>
                    <div class="shape-icon">
                        <img src="{{asset('assetsuser/images/flag/spa.png')}}" style="width: 60px" alt="Ikon">
                        <span class="dots"></span>
                    </div>
                    <h3 class="box-title">
                        <a href="/training/spa-training">Pelatihan SPA</a>
                    </h3>
                    <div class="bg-shape">
                        <img src="assetsuser/img/bg/service_card_bg.png" alt="bg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="position-relative space">
    <div class="th-bg-img" data-bg-src="assetsuser/img/bg/cta_bg_2.jpg"><img src="assetsuser/img/bg/bg_overlay_1.png"
            alt="overlay"></div>
    <div class="container z-index-common">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-7 col-md-9 text-center">
                <div class="title-area mb-35"><span class="sub-title">
                        <div class="icon-masking me-2"><span class="mask-icon"
                                data-mask-src="assetsuser/img/theme-img/title_shape_2.svg"></span> <img
                                src="assetsuser/img/theme-img/title_shape_2.svg" alt="shape"></div>HALO
                    </span>
                    <h2 class="sec-title text-white">Ingin bergabung <br> <span
                            class="text-theme fw-normal">dengan Kami?</span></h2>
                </div><a href="/contact" class="th-btn style3">Hubungi Sekarang</a>
            </div>
        </div>
    </div>
</section>
<section class="bg-smoke" id="process-sec" data-bg-src="assetsuser/img/bg/process_bg_1.png">
    <div class="container space">
        <div class="title-area text-center"><span class="sub-title">
                <div class="icon-masking me-2"><span class="mask-icon"
                        data-mask-src="assetsuser/img/theme-img/title_shape_2.svg"></span> <img
                        src="assetsuser/img/theme-img/title_shape_2.svg" alt="shape"></div>DETAIL PROSES
            </span>
            <h2 class="sec-title">Rangkaian Proses</h2>
        </div>
        <div class="process-card-area">
            <div class="row gy-40">
                <div class="col-sm-6 col-xl-3 process-card-wrap">
                    <div class="process-card">
                        <div class="process-card_number">01</div>
                        <h2 class="box-title">Konsultasi</h2>
                        <p class="process-card_text">Whatsapp / Kantor Online / Offline</p>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3 process-card-wrap">
                    <div class="process-card">
                        <div class="process-card_number">02</div>
                        <h2 class="box-title">Analisis</h2>
                        <p class="process-card_text">Mengisi Kuesioner dan Tes Psikologis</p>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3 process-card-wrap">
                    <div class="process-card">
                        <div class="process-card_number">03</div>
                        <h2 class="box-title">Pelatihan</h2>
                        <p class="process-card_text">Klien diwajibkan mengikuti pelatihan di LPK Bali Sanitya Sejahtera</p>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3 process-card-wrap">
                    <div class="process-card">
                        <div class="process-card_number">04</div>
                        <h2 class="box-title">Pendaftaran</h2>
                        <p class="process-card_text">Klayen melakukan pembayaran biaya pendaftaran</p>
                    </div>
                </div>
            </div>
            <div class="row gy-40">
                <div class="col-sm-6 col-xl-3 process-card-wrap">
                    <div class="process-card">
                        <div class="process-card_number">05</div>
                        <h2 class="box-title">Wawancara</h2>
                        <p class="process-card_text">Klayen harus mengikuti seleksi wawancara</p>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3 process-card-wrap">
                    <div class="process-card">
                        <div class="process-card_number">06</div>
                        <h2 class="box-title">Dokumen</h2>
                        <p class="process-card_text">Klayen melengkapi persyaratan dokumen & tahap pembayaran pertama</p>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3 process-card-wrap">
                    <div class="process-card">
                        <div class="process-card_number">07</div>
                        <h2 class="box-title">Proses Visa</h2>
                        <p class="process-card_text">Semua dokumen dari pengguna dan klien akan diproses di kedutaan</p>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3 process-card-wrap">
                    <div class="process-card">
                        <div class="process-card_number">08</div>
                        <h2 class="box-title">Berangkat</h2>
                        <p class="process-card_text">Keluar Visa, Pembayaran Pembayaran, Pembelian Tiket, OPP/E-KTKLN, Berangkat</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

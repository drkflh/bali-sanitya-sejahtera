@extends('layouts.app')

@section('title', "Admin Gallery")

@section('content')
<div class="container-fluid">

    <h1 class="h3 mb-2 text-gray-800">Gallery</h1>
    <p class="mb-4">Isikan Gambar Dibawah Untuk Menampilkan Foto Foto Di Tampilan User Gallery.</p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Gallery</h6>
        </div>
        <div class="card-body">
            <button class="btn btn-primary mb-3" id="addImageBtn">Add Image</button>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Foto</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Foto</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($galleries as $gallery)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td><img src="{{ asset('images/gallery/' . $gallery->photo) }}" alt="Photo" width="300"></td>
                            <td>{{ $gallery->is_active ? 'Enabled' : 'Disabled' }}</td>
                            <td>
                                <button class="btn btn-warning btn-sm edit-btn" data-id="{{ $gallery->id }}" data-photo="{{ asset('images/gallery/' . $gallery->photo) }}">Edit</button>
                                <form action="{{ route('galleries.destroy', $gallery->id) }}" method="POST" style="display:inline;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                </form>
                                <form action="{{ route('galleries.toggle', $gallery->id) }}" method="POST" style="display:inline;">
                                    @csrf
                                    @method('PATCH')
                                    <button type="submit" class="btn btn-secondary btn-sm">{{ $gallery->is_active ? 'Disable' : 'Enable' }}</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- Add Image Modal -->
<div class="modal fade" id="addImageModal" tabindex="-1" role="dialog" aria-labelledby="addImageModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addImageModalLabel">Add New Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('galleries.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="photo">Photo</label>
                        <input type="file" class="form-control" id="photo" name="photo" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editImageModal" tabindex="-1" role="dialog" aria-labelledby="editImageModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editImageModalLabel">Edit Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST" enctype="multipart/form-data" id="editImageForm">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="editPhoto">Photo</label>
                        <input type="file" class="form-control" id="editPhoto" name="photo">
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    document.getElementById('addImageBtn').addEventListener('click', function() {
        $('#addImageModal').modal('show');
    });

    document.querySelectorAll('.edit-btn').forEach(button => {
        button.addEventListener('click', function() {
            const galleryId = this.dataset.id;
            const photoUrl = this.dataset.photo;
            const formAction = `{{ url('galleries') }}/${galleryId}`;
            document.getElementById('editImageForm').action = formAction;
            $('#editImageModal').modal('show');
        });
    });
</script>
@endsection

@extends('layouts.app')

@section('title', "Admin Contact")

@section('content')
<div class="container-fluid">

    <h1 class="h3 mb-2 text-gray-800">Pesan Contact</h1>
    <p class="mb-4">Ini merupakan Pesan Dari User Dihalaman Contact Us</p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Pesan Contact</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Awalan</th>
                            <th>Nama Akhiran</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Pesan</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama Awalan</th>
                            <th>Nama Akhiran</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Pesan</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($contact as $contactloop)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $contactloop->namaawalan }}</td>
                            <td>{{ $contactloop->namaakhiran }}</td>
                            <td>{{ $contactloop->email }}</td>
                            <td>{{ $contactloop->nomorhp }}</td>
                            <td>{{ $contactloop->pesan }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
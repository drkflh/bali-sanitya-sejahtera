@extends('layouts.app')

@section('title', "Admin Blog")

@section('content')
<div class="container-fluid">

    <h1 class="h3 mb-2 text-gray-800">Nama Kategori</h1>
    <p class="mb-4">Isikan Nama Kategori Untuk Menambahkan Categori Blog</p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Kategori</h6>
        </div>
        <div class="card-body">
            <button class="btn btn-primary mb-3" id="addImageBtn">Add Kategori</button>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @if ($categories->isEmpty())
                        <tr>
                            <td colspan="3">Tidak ada kategori yang tersedia.</td>
                        </tr>
                        @else
                        @foreach ($categories as $category)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $category->nama_kategori }}</td>
                            <td>
                                <button class="btn btn-warning btn-sm edit-btn"
                                        data-id="{{ $category->id }}"
                                        data-nama-kategori="{{ $category->nama_kategori }}">Edit</button>
                        
                                <form action="{{ route('category.destroy', $category->id) }}" method="POST" style="display:inline;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        
                        @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="addImageModal" tabindex="-1" role="dialog" aria-labelledby="addImageModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addImageModalLabel">Add New Blog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('category.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="nama_kategori">Nama Kategori</label>
                        <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editImageModal" tabindex="-1" role="dialog" aria-labelledby="editImageModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editImageModalLabel">Edit Kategori</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editForm" action="{{ route('category.update', ['category' => $category->id]) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" id="editKategoriId" name="id">
                    <div class="form-group">
                        <label for="editNamaKategori">Nama Kategori</label>
                        <input type="text" class="form-control" id="editNamaKategori" name="nama_kategori" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    document.getElementById('addImageBtn').addEventListener('click', function() {
        $('#addImageModal').modal('show');
    });

    document.querySelectorAll('.edit-btn').forEach(button => {
        button.addEventListener('click', function() {
            const kategoriId = this.dataset.id;
            const namaKategori = this.dataset.namaKategori;

            document.getElementById('editKategoriId').value = kategoriId;
            document.getElementById('editNamaKategori').value = namaKategori;

            $('#editImageModal').modal('show');
        });
    });
</script>
@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endpush

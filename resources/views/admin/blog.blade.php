@extends('layouts.app')

@section('title', "Admin Blog")

@section('content')
<div class="container-fluid">

    <h1 class="h3 mb-2 text-gray-800">Blog</h1>
    <p class="mb-4">Isikan Judul, Deskripsi, Gambar Dibawah Untuk Menampilkan Berita Di Tampilan User Blog.</p>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Blog</h6>
        </div>
        <div class="card-body">
            <button class="btn btn-primary mb-3" id="addImageBtn">Add Blog</button>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Gambar</th>
                            <th>Kategori</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Gambar</th>
                            <th>Kategori</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($blogadmin as $blogadminloop)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $blogadminloop->judul }}</td>
                            <td>{!! html_entity_decode($blogadminloop->deskripsi ) !!}</td>
                            <td><img src="{{ asset('images/blog/' . $blogadminloop->photo) }}" alt="Photo" width="300"></td>
                            <td>{{ $blogadminloop->category->nama_kategori }}</td>
                            <td>{{ $blogadminloop->is_active ? 'Enabled' : 'Disabled' }}</td>
                            <td>
                                <button class="btn btn-warning btn-sm edit-btn" data-id="{{ $blogadminloop->id }}"
                                    data-judul="{{ $blogadminloop->judul }}"
                                    data-deskripsi="{!! $blogadminloop->deskripsi !!}"
                                    data-photo="{{ asset('images/blog/' . $blogadminloop->photo) }}">Edit</button>
                                <form action="{{ route('blog.destroy', $blogadminloop->id) }}" method="POST"
                                    style="display:inline;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                </form>
                                <form action="{{ route('blog.toggle', $blogadminloop->id) }}" method="POST"
                                    style="display:inline;">
                                    @csrf
                                    @method('PATCH')
                                    <button type="submit" class="btn btn-secondary btn-sm">{{ $blogadminloop->is_active
                                        ? 'Disable' : 'Enable' }}</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="addImageModal" tabindex="-1" role="dialog" aria-labelledby="addImageModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addImageModalLabel">Add New Blog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('blog.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" required>
                    </div>
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea class="ckeditor form-control" id="deskripsi" name="deskripsi"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="photo">Photo</label>
                        <input type="file" class="form-control" id="photo" name="photo" required>
                    </div>
                    <div class="form-group">
                        <label for="category_id">Kategori</label>
                        <select class="form-control" id="category_id" name="category_id" required>
                            @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->nama_kategori }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editImageModal" tabindex="-1" role="dialog" aria-labelledby="editImageModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editImageModalLabel">Edit Blog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST" enctype="multipart/form-data" id="editImageForm">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="editJudul">Judul</label>
                        <input type="text" class="form-control" id="editJudul" name="judul" required>
                    </div>
                    <div class="form-group">
                        <label for="editDeskripsi">Deskripsi</label>
                        <textarea class="ckeditor form-control" id="editDeskripsi" name="deskripsi"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="editPhoto">Photo</label>
                        <input type="file" class="form-control" id="editPhoto" name="photo">
                    </div>
                    <div class="form-group">
                        <label for="editCategory">Kategori</label>
                        <select class="form-control" id="editCategory" name="category_id" required>
                            @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->nama_kategori }}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                    <div id="currentPhotoContainer" style="display: none;">
                        <img id="currentPhoto" src="" alt="Current Photo" style="max-width: 100px;">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script>
    document.getElementById('addImageBtn').addEventListener('click', function() {
        $('#addImageModal').modal('show');
    });

    document.querySelectorAll('.edit-btn').forEach(button => {
    button.addEventListener('click', function() {
        const blogId = this.dataset.id;
        const judul = this.dataset.judul;
        const deskripsi = this.dataset.deskripsi;
        const photo = this.dataset.photo;
        const categoryId = this.dataset.categoryId; 

        const formAction = `{{ url('admin/blog') }}/${blogId}`;

        document.getElementById('editImageForm').action = formAction;
        document.getElementById('editJudul').value = judul;

        if (typeof deskripsi !== 'undefined' && deskripsi.trim() !== '') {
            document.getElementById('editDeskripsi').value = deskripsi;
        } else {
            document.getElementById('editDeskripsi').value = ''; 
        }

        if (photo) {
            document.getElementById('currentPhoto').src = photo;
            document.getElementById('currentPhotoContainer').style.display = 'block';
        } else {
            document.getElementById('currentPhotoContainer').style.display = 'none';
        }

        document.getElementById('editCategory').value = categoryId;

        $('#editImageModal').modal('show');
    });
});
</script>
@endsection

@push('scripts')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endpush

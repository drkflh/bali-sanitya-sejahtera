@extends('layouts.app')

@section('title', "Data Visitor")

@section('content')
<div class="container-fluid">

    <h1 class="h3 mb-2 text-gray-800">Data Visitor</h1>
    <p class="mb-4">Ini merupakan daftar pengunjung yang telah mengunjungi website Anda.</p>


    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Pengunjung</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">    
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>IP Address</th>
                            <th>Visited At</th>
                            <th>Platform</th>
                            <th>Device Type</th>
                            <th>Browser</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>IP Address</th>
                            <th>Visited At</th>
                            <th>Platform</th>
                            <th>Device Type</th>
                            <th>Browser</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($visitors as $visitor)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $visitor->ip_address }}</td>
                            <td>{{ \Carbon\Carbon::parse($visitor->visited_at)->timezone('Asia/Jakarta')->format('d-m-Y H:i:s') }}</td>
                            <td>{{ $visitor->platform }}</td>
                            <td>{{ $visitor->device_type }}</td>
                            <td>{{ $visitor->browser }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
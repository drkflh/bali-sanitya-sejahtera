@extends('layouts.guest')

@section('title', "Pelatihan Bahasa korea")

@section('content')
<div class="breadcumb-wrapper" data-bg-src="{{asset('assetsuser/images/Trainingkorea.png')}}">
    <div class="container">
        <div class="breadcumb-content">
            <h1 class="breadcumb-title">Pelatihan Bahasa Korea</h1>
            <ul class="breadcumb-menu">
                <li><a href="/">Beranda</a></li>
                <li>Pelatihan Bahasa Korea</li>
            </ul>
        </div>
    </div>
</div>
<section class="space-top space-extra-bottom">
    <div class="container">
        <div class="row">
            <div class="col-xxl-8 col-lg-8">
                <div class="page-single">
                    <div class="page-content">
                        <h2 class="h3 page-title">Pelatihan Bahasa Korea</h2>
                        <p class="">Kursus ini membantu peserta mempersiapkan diri untuk Test of Proficiency in Korean (TOPIK), yang diperlukan untuk keperluan akademis atau profesional di Korea. Fokus pada latihan membaca, menulis, mendengarkan, dan tata bahasa.</p>
                        <div class="project-inner-box mb-40">
                            <h3 class="box-title">Fitur Program Pelatihan Bahasa</h3>
                            <p class="">1. Guru Profesional</p>
                            <div class="row gy-4 align-items-center">
                                <div class="col-md-10">
                                    <div class="checklist">
                                        <ul>
                                            <li><i class="fas fa-badge-check"></i>Instruktur dengan pengalaman dan keahlian dalam mengajar bahasa korea sebagai bahasa asing, termasuk penutur asli bahasa korea.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <p class="">2. Kurikulum Terstruktur</p>
                            <div class="row gy-4 align-items-center">
                                <div class="col-md-10">
                                    <div class="checklist">
                                        <ul>
                                            <li><i class="fas fa-badge-check"></i>Kurikulum ini tersusun secara sistematis untuk sesuai dengan tingkat kecakapan dan tujuan belajar peserta. Peserta akan diberikan buku modul pembelajaran yang mendukung proses belajar.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <p class="">3. Fasilitas Lengkap</p>
                            <div class="row gy-4 align-items-center">
                                <div class="col-md-10">
                                    <div class="checklist">
                                        <ul>
                                            <li><i class="fas fa-badge-check"></i>Ruang kelas yang nyaman dengan fasilitas pendukung untuk pengalaman belajar yang optimal.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <p class="">4. Sertifikasi</p>
                            <div class="row gy-4 align-items-center">
                                <div class="col-md-10">
                                    <div class="checklist">
                                        <ul>
                                            <li><i class="fas fa-badge-check"></i>Sertifikat diberikan kepada peserta yang berhasil menyelesaikan kursus ini, yang dapat digunakan sebagai bukti kemampuan dalam bahasa korea.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="project-inner-box">
                            <h3 class="box-title">Bagaimana cara mendaftar?</h3>
                            <p class="mb-3">1. Registrasi Online <br>
                                Isi data secara lengkap di Google Form Melalui Link Ini <a href="https://forms.gle/VYk4DMJRfVH8dByv8">Click Daftar Online</a></p>
                            <p class="mb-3">2. Registrasi Langsung<br>
                                Langsung datang ke kantor kami di: <br>
                                Jl. Raya Baturaden No.202 Pabuaran Purwokerto Utara Banyumas
                            </p>
                            <br>
                            <h3 class="box-title">Konsultasi Gratis</h3>
                            <h1 class="box-title" style="color: green;">
                                <i class="fab fa-whatsapp"></i>
                                <a href="https://wa.me/6282266489324" style="color: inherit; text-decoration: none;">+62 8226 6489 324</a>
                            </h1>                            
                        </div>
                        <h3 class="h4 mt-35 mb-4">Pertanyaan Tentang Pelatihan</h3>
                        <div class="accordion-area accordion" id="faqAccordion">
                            <div class="accordion-card style2 active">
                                <div class="accordion-header" id="collapse-item-1"><button class="accordion-button"
                                        type="button" data-bs-toggle="collapse" data-bs-target="#collapse-1"
                                        aria-expanded="true" aria-controls="collapse-1">Siapa yang bisa mengikuti kelas pelatihan bahasa korea ini?</button></div>
                                <div id="collapse-1" class="accordion-collapse collapse show"
                                    aria-labelledby="collapse-item-1" data-bs-parent="#faqAccordion">
                                    <div class="accordion-body">
                                        <p class="faq-text">Kelas ini terbuka untuk siapa saja yang ingin belajar bahasa korea, baik pemula maupun yang ingin meningkatkan keterampilan bahasa korea mereka untuk tujuan profesional.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-card style2">
                                <div class="accordion-header" id="collapse-item-2"><button
                                        class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse-2" aria-expanded="false"
                                        aria-controls="collapse-2">Materi apa yang akan diajarkan dalam kelas ini?</button>
                                </div>
                                <div id="collapse-2" class="accordion-collapse collapse"
                                    aria-labelledby="collapse-item-2" data-bs-parent="#faqAccordion">
                                    <div class="accordion-body">
                                        <p class="faq-text">Materi meliputi dasar-dasar bahasa korea, percakapan sehari-hari, bahasa korea untuk tujuan profesional, serta budaya korea dan etika kerja.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-card style2">
                                <div class="accordion-header" id="collapse-item-3"><button
                                        class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse-3" aria-expanded="false"
                                        aria-controls="collapse-3">Siapa saja guru-guru dalam program pelatihan ini?</button>
                                </div>
                                <div id="collapse-3" class="accordion-collapse collapse"
                                    aria-labelledby="collapse-item-3" data-bs-parent="#faqAccordion">
                                    <div class="accordion-body">
                                        <p class="faq-text">Guru-guru kami adalah profesional berpengalaman dalam mengajar bahasa korea dan memahami budaya serta etika kerja di korea. Mereka memiliki sertifikasi yang relevan dan pengalaman praktis.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-card style2">
                                <div class="accordion-header" id="collapse-item-4"><button
                                        class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapse-4" aria-expanded="false"
                                        aria-controls="collapse-4">Apakah peserta akan menerima sertifikat setelah menyelesaikan pelatihan?</button>
                                </div>
                                <div id="collapse-4" class="accordion-collapse collapse"
                                    aria-labelledby="collapse-item-4" data-bs-parent="#faqAccordion">
                                    <div class="accordion-body">
                                        <p class="faq-text">Ya,</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-4 col-lg-4">
                <aside class="sidebar-area">
                    <div class="widget widget_info">
                        <h3 class="widget_title">Informasi Pelatihan</h3>
                        <div class="project-info-list">
                            <div class="contact-feature">
                                <div class="icon-btn"><i class="fa-solid fa-school"></i></div>
                                <div class="media-body">
                                    <p class="contact-feature_label">Judul :</p><a class="contact-feature_link">Pelatihan Bahasa korea</a>
                                </div>
                            </div>
                            <div class="contact-feature">
                                <div class="icon-btn"><i class="fa-solid fa-folder-open"></i></div>
                                <div class="media-body">
                                    <p class="contact-feature_label">Kategori:</p><a class="contact-feature_link">Pelatihan</a>
                                </div>
                            </div>
                            <div class="contact-feature">
                                <div class="icon-btn"><i class="fa-solid fa-location-dot"></i></div>
                                <div class="media-body">
                                    <p class="contact-feature_label">Alamat:</p><a class="contact-feature_link">Denpasar, Bali</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget widget_banner" data-bg-src="assets/img/bg/widget_banner.jpg">
                        <div class="widget-banner"><span class="text">HUBUNGI KAMI SEKARANG</span>
                            <h2 class="title">Butuh Bantuan?</h2><a href="https://wa.me/6282266489324" class="th-btn style3">TELP SEKARANG<i class="fas fa-arrow-right ms-2"></i></a>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</section>
@endsection

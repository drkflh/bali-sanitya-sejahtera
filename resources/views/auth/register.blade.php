
@extends('layouts.guest')

@section('title', "Register")

@section('content')
<br>
<div class="container">
    <div class="woocommerce-form-login-toggle">
        <div class="woocommerce-info">Register New Account Admin</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form method="POST" action="{{ route('register') }}" class="woocommerce-form-login">
                @csrf
                <div class="form-group"><label>Username *</label> <input type="text"
                        class="form-control" placeholder="Name" name="name"></div>
                <div class="form-group"><label>Email *</label> <input type="email"
                        class="form-control" placeholder="Email" name="email"></div>
                <div class="form-group"><label>Password *</label> <input type="password" class="form-control"
                        placeholder="Password" name="password"></div>
                <div class="form-group"><label>Confirm Password *</label> <input type="password" class="form-control"
                        placeholder="Password" name="password_confirmation"></div>
                <div class="form-group">
                    <div class="custom-checkbox"><input type="checkbox" id="remembermylogin"> <label
                            for="remembermylogin">Remember Me</label></div>
                </div>
                <div class="form-group">
                    <button type="submit" class="th-btn">Register</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@extends('layouts.guest')

@section('title', "Login")

@section('content')
<br>
<div class="container">
    <div class="woocommerce-form-login-toggle">
        <div class="woocommerce-info">Welcome Back Admin</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form method="POST" action="{{ route('login') }}" class="woocommerce-form-login">
                @csrf
                <div class="form-group"><label>Email *</label> <input type="email"
                        class="form-control" placeholder="Email" name="email"></div>
                <div class="form-group"><label>Password *</label> <input type="password" class="form-control"
                        placeholder="Password" name="password"></div>
                <div class="form-group">
                    <div class="custom-checkbox"><input type="checkbox" id="remembermylogin"> <label
                            for="remembermylogin">Remember Me</label></div>
                </div>
                <div class="form-group">
                    <button type="submit" class="th-btn">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@extends('layouts.guest')

@section('title', "Gallery")

@section('content')
<div class="breadcumb-wrapper" style="position: relative; background-image: url('{{ asset('assetsuser/images/head_bg.jpg') }}'); background-size: cover; background-position: center; height: 100%;">
    <div class="overlay" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); z-index: 1;"></div>
    <div class="breadcumb-content" style="position: relative; z-index: 2; color: white;">
        <h1 class="breadcumb-title" style="color: white;">Galeri Kami</h1>
        <br>
        <ul class="breadcumb-menu" style="list-style: none; padding: 0; margin: 0;">
            <li style="display: inline; margin-right: 10px;"><a href="/" style="color: white; text-decoration: none;">Beranda</a></li>
            <li style="display: inline;">Galeri Kami</li>
        </ul>
    </div>
</div>
<div class="space-top space-extra-bottom">
<div class="container">
    <div class="row gy-4">
        @foreach ($galleryuser as $galleryuserloop)
        <div class="col-md-6 col-xl-4">
            <div class="gallery-card">
                <div class="gallery-img"><img src="{{ asset('images/gallery/' . $galleryuserloop->photo) }}" alt="gallery image"> <a
                        href="{{ asset('images/gallery/' . $galleryuserloop->photo) }}" class="play-btn style3 popup-image"><i
                            class="far fa-plus"></i></a></div>
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>

@endsection

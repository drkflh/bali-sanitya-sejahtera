@extends('layouts.guest')

@section('title', "Tentang")

@section('content')
<div class="breadcumb-wrapper" style="position: relative; background-image: url('{{ asset('assetsuser/images/head_bg.jpg') }}'); background-size: cover; background-position: center; height: 100%;">
    <div class="overlay" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); z-index: 1;"></div>
    <div class="breadcumb-content" style="position: relative; z-index: 2; color: white;">
        <h1 class="breadcumb-title" style="color: white;">Tentang Kami</h1>
        <br>
        <ul class="breadcumb-menu" style="list-style: none; padding: 0; margin: 0;">
            <li style="display: inline; margin-right: 10px;"><a href="/" style="color: white; text-decoration: none;">Beranda</a></li>
            <li style="display: inline;">Tentang Kami</li>
        </ul>
    </div>
</div>

<div class="overflow-hidden space" id="about-sec">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-6 mb-30 mb-xl-0">
                <div class="img-box1">
                    <div class="img1"><img src="{{asset('assetsuser/images/logo.png')}}" alt="Tentang"></div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="ps-xxl-4 ms-xl-3">
                    <div class="title-area mb-35"><span class="sub-title">
                            <div class="icon-masking me-2"><span class="mask-icon"
                                    data-mask-src="assetsuser/img/theme-img/title_shape_1.svg"></span> <img
                                    src="assetsuser/img/theme-img/title_shape_1.svg" alt="shape"></div>Tentang Kami
                        </span>
                        <h2 class="sec-title">Kami Meningkatkan Kesuksesan Karir dengan <span class="text-theme">Solusi
                                Pelatihan</span></h2>
                    </div>
                    <p class="mt-n2 mb-25">Yayasan Bali Sanitya Sejahtera merupakan organisasi yang fokus pada pelatihan
                        kerja. Tujuan kami adalah mempersiapkan individu dengan keterampilan yang sesuai dengan
                        kebutuhan industri. Kami mempunyai pengalaman sebagai organisasi pengirim dan menyediakan
                        program pelatihan yang komprehensif dan terkini.<br>



                    </p>
                    <p class="mt-n2 mb-25"> Selain memberikan pelatihan teknis, kami juga memberikan perhatian khusus
                        pada pengembangan soft skill yang penting dalam dunia kerja. Kami menggunakan pendekatan
                        inovatif dan kolaboratif untuk memastikan setiap peserta pelatihan mendapatkan pengalaman yang
                        relevan dan bermakna.<br>
                    </p>
                    <p class="mt-n2 mb-25"> Kami berkomitmen untuk menjadi mitra yang dapat diandalkan dalam membantu
                        peserta pelatihan kami membangun karier yang sukses dan berkelanjutan, dengan fokus pada
                        kualitas, keberlanjutan, dan kesuksesan individu.<br>
                    </p>
                    <p class="mt-n2 mb-25"> Bali Sanitya Sejahtera sudah resmi terdaftar dengan nomor SO (Organisasi
                        Pengirim) AHU-0002336.AH.01.04.Tahun 2020 KEMNAKER NO : 2/1645/HK.13/VII/2021. Kami merupakan
                        lembaga pelatihan swasta yang mempunyai izin resmi dalam menyelenggarakan pelatihan, dan
                        penyaluran pemagangan tenaga kerja Indonesia. Selain itu, lembaga ini menjadi wadah bagi calon
                        pekerja migran Indonesia untuk dapat mencari peluang kerja khususnya di luar negeri. Melalui
                        lembaga swasta yang memiliki izin resmi maka kualitas dan keselamatan serta kesejahteraan
                        pekerja migran akan terjamin.
                    </p>
                    <p class="mt-n2 mb-25">Motto Kami: BERPIKIR, BERAKSI, CERDAS, DAN SUKSES DENGAN HATI YANG MEMBERI
                    </p>
                    <div class="btn-group"><a href="https://wa.me/6282266489324" target="_blank" class="th-btn">Hubungi
                            Sekarang<i class="fa-regular fa-arrow-right ms-2"></i></a>
                        <div class="call-btn">
                            <div class="play-btn"><i class="fas fa-phone"></i></div>
                            <div class="media-body"><span class="btn-text">Hubungi Kami di:</span> <a
                                    href="https://wa.me/6282266489324" target="_blank" class="btn-title">+62 822 6648
                                    9324</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="space" data-bg-src="assetsuser/img/bg/why_bg_1.png">
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-xxl-7 col-xl-6 mb-30 mb-xl-0">
                <div class="img-box2">
                    <div class="img1"><img src="{{asset('assetsuser/images/aboutvisionmision.png')}}" alt="Mengapa">
                    </div>
                </div>
            </div>
            <div class="col-xxl-5 col-xl-6">
                <div class="title-area mb-35"><span class="sub-title">
                        <div class="icon-masking me-2"><span class="mask-icon"
                                data-mask-src="assetsuser/img/theme-img/title_shape_1.svg"></span> <img
                                src="assetsuser/img/theme-img/title_shape_1.svg" alt="shape"></div>Visi & Misi
                    </span>
                    <h2 class="sec-title">Berikut adalah visi & misi perusahaan kami</h2>
                </div>
                <div class="two-column">
                    <div class="checklist style2">
                        <ul>
                            <li>Visi :</li>
                            <li>Menjadi perusahaan penempatan pekerja migran yang terpercaya dan terkemuka yang fokus
                                pada kesejahteraan dan keselamatan pekerja migran Indonesia di luar negeri serta
                                berkontribusi pada pembangunan ekonomi negara.</li>
                        </ul>
                    </div>
                    <div class="checklist style2">
                        <ul>
                            <li>Misi :</li>
                            <li>• Membangun Kemitraan Berkualitas</li>
                            <li>• Rekrutmen dan Seleksi Berkualitas</li>
                            <li>• Pelatihan dan Persiapan</li>
                            <li>• Perlindungan dan Hak-hak Pekerja</li>
                            <li>• Layanan Selama Penempatan</li>
                            <li>• Keterlibatan dengan Keluarga</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="bg-top-right overflow-hidden space-bottom" id="blog-sec" data-bg-src="assetsuser/img/bg/blog_bg_1.png">
    <div class="container space-bottom">
        <div class="title-area text-center"><span class="sub-title">
                <div class="icon-masking me-2"><span class="mask-icon"
                        data-mask-src="assetsuser/img/theme-img/title_shape_1.svg"></span> <img
                        src="assetsuser/img/theme-img/title_shape_1.svg" alt="shape"></div>BOG
            </span>
            <h2 class="sec-title">Dapatkan <span class="text-theme">Berita</span> Terbaru Dari Kami</h2>
        </div>
        <div class="slider-area">
            <div class="swiper th-slider has-shadow" id="blogSlider1"
                data-slider-options='{"loop":true,"breakpoints":{"0":{"slidesPerView":1},"576":{"slidesPerView":"1"},"768":{"slidesPerView":"2"},"992":{"slidesPerView":"2"},"1200":{"slidesPerView":"3"}}}'>
                <div class="swiper-wrapper">
                    @foreach($bloguser as $user)
                    <div class="swiper-slide">
                        <div class="blog-card">
                            <div class="blog-img"><img src="{{ asset('images/blog/' . $user->photo) }}"
                                    alt="gambar blog"></div>
                            <div class="blog-content">
                                <div class="blog-meta"><a href="#"><i class="fal fa-calendar-days"></i>15
                                        Jan, 2024</a></div>
                                <h3 class="box-title"><a href="{{ route('blog.detail', ['slug' => $user->slug]) }}">{{
                                        $user->judul }}</a></h3>
                                <div class="blog-bottom"><a href="#" class="author"><img
                                            src="{{asset('assetsuser/images/logo.png')}}" width="30px" alt="avater">
                                        Oleh Admin</a> <a href="{{ route('blog.detail', ['slug' => $user->slug]) }}"
                                        class="line-btn">Baca Selengkapnya<i class="fas fa-arrow-right"></i></a></div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div><button data-slider-prev="#blogSlider1" class="slider-arrow style3 slider-prev"><i
                    class="far fa-arrow-left"></i></button> <button data-slider-next="#blogSlider1"
                class="slider-arrow style3 slider-next"><i class="far fa-arrow-right"></i></button>
        </div>
    </div>
    <div class="container">
        <div class="slider-area text-center">
            <div class="swiper th-slider"
                data-slider-options='{"breakpoints":{"0":{"slidesPerView":2},"576":{"slidesPerView":"2"},"768":{"slidesPerView":"3"},"992":{"slidesPerView":"3"},"1200":{"slidesPerView":"4"},"1400":{"slidesPerView":"5"}}}'>
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 100px;"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/australia.png')}}" alt="Australia"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 100px;"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/inggris.png')}}" alt="Inggris"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 100px;"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/korea.png')}}" alt="Korea"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 100px;"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/jepang.png')}}" alt="Jepang"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 100px;"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/kuwait.png')}}" alt="Kuwait"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/malaysia.png')}}" alt="Malaysia"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/arabsaudi.png')}}" alt="Arab Saudia"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 110px;"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/poland.png')}}" alt="Polandia"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shape-mockup" data-bottom="0" data-left="0">
        <div class="particle-2 small" id="particle-4"></div>
    </div>
</section>
@endsection

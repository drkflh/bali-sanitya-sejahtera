<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bali Sanitya Sejahtera - @yield('title')</title>
    <meta name="author" content="IT Bali Sanitya Sejahtera">
    <meta name="description" content="LPK Bali Sanitya Sejahtera adalah lembaga pelatihan kerja yang terkemuka, menyediakan program pelatihan berbasis industri untuk mempersiapkan individu dalam memasuki pasar kerja global.">
    <meta name="keywords" content="pelatihan kerja Bali, LPK Bali, teknisi elektronik, keperawatan internasional, kuliner internasional, pelatihan bahasa Jepang Bali, pelatihan bahasa Korea Bali, karir internasional, bimbingan karir LPK, jaringan alumni LPK">
    <meta name="robots" content="INDEX,FOLLOW">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assetsuser/images/logo.png')}}">
    <link rel="manifest" href="assetsuser/img/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('assetsuser/images/logo.png')}}">
    <meta name="theme-color" content="#ffffff">
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,700&amp;family=Roboto:wght@300;400;500;700;900&amp;display=swap"
        rel="stylesheet">


    <link rel="stylesheet" href="{{asset('assetsuser/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/magnific-popup.min.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/swiper-bundle.min.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/style.css')}}">
</head>

<body class="gray-body">
    <div class="cursor"></div>
    <div class="cursor2"></div>

    @include('partials.guest.header')
    @yield('content')
    @include('partials.guest.footer')


    <div class="scroll-top"><svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"
                style="transition: stroke-dashoffset 10ms linear 0s; stroke-dasharray: 307.919, 307.919; stroke-dashoffset: 307.919;">
            </path>
        </svg></div>

    <script src="{{asset('assetsuser/js/vendor/jquery-3.7.1.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/swiper-bundle.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/circle-progress.js')}}"></script>
    <script src="{{asset('assetsuser/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/tilt.jquery.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/gsap.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/ScrollTrigger.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/smooth-scroll.js')}}"></script>
    <script src="{{asset('assetsuser/js/particles.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/particles-config.js')}}"></script>
    <script src="{{asset('assetsuser/js/main.js')}}"></script>
</body>
</html>
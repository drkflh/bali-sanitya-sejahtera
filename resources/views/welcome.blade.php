@extends('layouts.guest')

@section('title', "Dashboard")

@section('content')
<div class="th-hero-wrapper hero-6" id="hero">
    <div class="swiper th-slider" id="heroSlide3" data-slider-options='{"effect":"fade"}'>
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="hero-inner">
                    <div class="th-hero-bg" data-bg-src="{{asset('assetsuser/images/head_bg.jpg')}}"><img
                            src="{{asset('assetsuser/images/head_bg.jpg')}}" alt="Hero Image"></div>
                    <div class="container">
                        <div class="hero-style6"><span class="sub-title" data-ani="slideinup" data-ani-delay="0.5s">BALI
                                SANITYA SEJAHTERA FOUNDATION </span>
                            <h1 class="hero-title" data-ani="slideinup" data-ani-delay="0.6s">TRAINING CENTER & SENDING
                                ORGANIZATION
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="hero-inner">
                    <div class="th-hero-bg" data-bg-src="{{asset('assetsuser/images/bg_gallery.png')}}"><img
                            src="{{asset('assetsuser/images/bg_gallery.png')}}" alt="Hero Image"></div>
                    <div class="container">
                        <div class="hero-style6"><span class="sub-title" data-ani="slideinup"
                                data-ani-delay="0.5s">Company Principles</span>
                            <h1 class="hero-title" data-ani="slideinup" data-ani-delay="0.6s">Integrity, Trust,
                                Humanity, Safety, Professionalism
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="hero-inner">
                    <div class="th-hero-bg" data-bg-src="{{asset('assetsuser/images/bg_contact.png')}}"><img
                            src="{{asset('assetsuser/images/bg_contact.png')}}" alt="Hero Image"></div>
                    <div class="container">
                        <div class="hero-style6"><span class="sub-title" data-ani="slideinup" data-ani-delay="0.5s">Our
                                Motto
                            </span>
                            <h1 class="hero-title" data-ani="slideinup" data-ani-delay="0.6s">Think, Act, Be Smart,
                                Success, With a Giving Heart</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><button data-slider-prev="#heroSlide3" class="slider-arrow slider-prev"><i
            class="far fa-arrow-left"></i></button> <button data-slider-next="#heroSlide3"
        class="slider-arrow slider-next"><i class="far fa-arrow-right"></i></button>
</div>



<div class="overflow-hidden space" id="about-sec">
    <div class="container th-container4">
        <div class="about-area5">
            <div class="row">
                <div class="col-xl-6">
                    <div class="pe-xl-5 me-xl-4">
                        <div class="title-area mb-30"><span class="sub-title">Tentang Kami</span>
                            <h2 class="sec-title">Kami Meningkatkan Kesuksesan Karir dengan Solusi Pelatihan</h2>
                        </div>
                        <p class="mt-n2 mb-25">Yayasan Bali Sanitya Sejahtera adalah sebuah organisasi yang berfokus
                            pada pelatihan kerja. Tujuan kami adalah mempersiapkan individu dengan keterampilan yang
                            sesuai dengan kebutuhan industri. Kami memiliki pengalaman sebagai organisasi pengirim dan
                            menyediakan program pelatihan komprehensif dan terkini. Selain menyediakan pelatihan teknis,
                            kami juga memperhatikan pengembangan keterampilan lunak yang penting dalam dunia kerja. Kami
                            menggunakan pendekatan inovatif dan kolaboratif untuk memastikan setiap peserta didik
                            memiliki pengalaman yang relevan dan bermakna. Kami berkomitmen untuk menjadi mitra yang
                            dapat diandalkan dalam membantu peserta didik kami membangun karir yang sukses dan
                            berkelanjutan, dengan fokus pada kualitas, keberlanjutan, dan kesuksesan individu.</p>
                        <div class="about-btn mt-50"><a href="/about" class="th-btn style-radius"></i>Lihat Tentang
                                Kami</a></div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="img-box6">
                        <div class="img1"><img src="{{asset('assetsuser/images/dashboard.png')}}" style="width: 550px"
                                alt="Tentang"></div>
                        <div class="th-experience dance">
                            <div class="th-experience_content">
                                <h2 class="experience-year"><span class="counter-number" id="experience-year"></span>
                                </h2>
                                <p class="experience-text">Tahun pengalaman dalam Pusat Pelatihan & Organisasi Pengirim
                                </p>

                                <script>
                                    const currentYear = new Date().getFullYear();
                                    const startYear = 2019;
                                    const experience = currentYear - startYear;
                                    document.getElementById('experience-year').textContent = experience;
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <section class="overflow-hidden">
            <div class="container th-container4">
                <div class="cta-sec6 background-image bg-theme" data-bg-src="assetsuser/img/bg/cta_bg_6.jpg">
                    <div class="cta-content">
                        <div class="cta-wrapper">
                            <div class="cta-icon"><a href="tel:+1235859459"><img src="assetsuser/img/icon/call.svg"
                                        alt=""></a>
                            </div>
                            <div class="media-body"><span class="header-info_label text-white">Hubungi Untuk Informasi
                                    Lebih Lanjut</span>
                                <p class="header-info_link"><a href="https://wa.me/6282266489324"
                                        target="_blank">6282266489324</a></p>
                            </div>
                        </div>
                        <div class="title-area mb-0">
                            <h4 class="sec-title text-white">Ayo Ajukan Jadwal Konsultasi Gratis Sekarang</h4>
                        </div>
                        <div class="cta-group"><a href="https://wa.me/6282266489324" target="_blank"
                                class="th-btn th-border style-radius">Hubungi Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>


<section class="service-area7 m-4 mt-0 space" id="service-sec">
    <div class="container th-container4">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="title-area text-center">
                    <span class="sub-title">Pelatihan Kami</span>
                    <h2 class="sec-title">Berikut adalah daftar pelatihan kami</h2>
                </div>
            </div>
        </div>
        <div class="row row-cols-md-2 gy-4">
            <div class="col">
                <div class="service-card">
                    <div class="service-card_number">01</div>
                    <div class="shape-icon">
                        <img src="{{asset('assetsuser/images/flag/jepang.png')}}" style="width: 60px" alt="Ikon">
                        <span class="dots"></span>
                    </div>
                    <h3 class="box-title"><a href="/training/japanese-language-training">Pelatihan Bahasa Jepang</a>
                    </h3>
                    <div class="bg-shape"><img src="assetsuser/img/bg/service_card_bg.png" alt="bg"></div>
                </div>
            </div>
            <div class="col">
                <div class="service-card">
                    <div class="service-card_number">02</div>
                    <div class="shape-icon">
                        <img src="{{asset('assetsuser/images/flag/korea.png')}}" style="width: 60px" alt="Ikon">
                        <span class="dots"></span>
                    </div>
                    <h3 class="box-title"><a href="/training/korea-language-training">Pelatihan Bahasa Korea</a></h3>
                    <div class="bg-shape"><img src="assetsuser/img/bg/service_card_bg.png" alt="bg"></div>
                </div>
            </div>
            <div class="col">
                <div class="service-card">
                    <div class="service-card_number">03</div>
                    <div class="shape-icon">
                        <img src="{{asset('assetsuser/images/flag/inggris.png')}}" style="width: 60px" alt="Ikon">
                        <span class="dots"></span>
                    </div>
                    <h3 class="box-title"><a href="/training/inggris-language-training">Pelatihan Bahasa Inggris</a>
                    </h3>
                    <div class="bg-shape"><img src="assetsuser/img/bg/service_card_bg.png" alt="bg"></div>
                </div>
            </div>
            <div class="col">
                <div class="service-card">
                    <div class="service-card_number">04</div>
                    <div class="shape-icon">
                        <img src="{{asset('assetsuser/images/flag/spa.png')}}" style="width: 60px" alt="Ikon">
                        <span class="dots"></span>
                    </div>
                    <h3 class="box-title"><a href="/training/spa-training">Pelatihan SPA</a></h3>
                    <div class="bg-shape"><img src="assetsuser/img/bg/service_card_bg.png" alt="bg"></div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="overflow-hidden bg-top-center th-radius3 m-4 mt-0 mb-0 space" id="testi-sec"
    data-bg-src="assetsuser/img/bg/testimonial_bg_5.jpg">
    <div class="container th-container4">
        <div class="row justify-content-lg-between justify-content-center align-items-end">
            <div class="col-xxl-4 col-xl-6">
                <div class="title-area text-center text-lg-start"><span
                        class="sub-title style1 text-white">testimoni</span>
                    <h2 class="sec-title text-white">Lihat apa yang dikatakan oleh mereka</h2>
                </div>
            </div>
            <div class="col-lg-auto d-none d-xl-block">
                <div class="sec-btn">
                    <div class="icon-box"><button data-slider-prev="#testiSlider5"
                            class="slider-arrow style2 default"><i class="far fa-arrow-left"></i></button> <button
                            data-slider-next="#testiSlider5" class="slider-arrow style2 default"><i
                                class="far fa-arrow-right"></i></button></div>
                </div>
            </div>
        </div>
        <div class="slider-area">
            <div class="swiper th-slider has-shadow" id="testiSlider5"
                data-slider-options='{"breakpoints":{"0":{"slidesPerView":1},"576":{"slidesPerView":"1"},"768":{"slidesPerView":"2"},"992":{"slidesPerView":"2"},"1200":{"slidesPerView":"3"}}}'>
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="testi-box5 th-ani">
                            <p class="testi-box5_text">“ Terimakasih kepada Bali Sanitya Global telah memberangkatkan
                                saya magang ke Jepang ”</p>
                            <div class="testi-box5_wrapper">
                                <div class="testi-box5_profile">
                                    <div class="testi-box5_info">
                                        <h3 class="box-title">Putu </h3>
                                    </div>
                                </div>
                                <div class="testi-quote"><img src="{{asset('assetsuser/img/icon/quote.svg')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="testi-box5 th-ani">
                            <p class="testi-box5_text">“ Saya merupakan salah satu kandidat WHV Program di Australia ”
                            </p>
                            <div class="testi-box5_wrapper">
                                <div class="testi-box5_profile">
                                    <div class="testi-box5_info">
                                        <h3 class="box-title">Kadek </h3>
                                    </div>
                                </div>
                                <div class="testi-quote"><img src="{{asset('assetsuser/img/icon/quote.svg')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="testi-box5 th-ani">
                            <p class="testi-box5_text">“ Terimakasih Bali Sanitya Global telah membantu proses
                                keberangkatan saya.

                                ”</p>
                            <div class="testi-box5_wrapper">
                                <div class="testi-box5_profile">
                                    <div class="testi-box5_info">
                                        <h3 class="box-title">Komang </h3>
                                    </div>
                                </div>
                                <div class="testi-quote"><img src="{{asset('assetsuser/img/icon/quote.svg')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="testi-box5 th-ani">
                            <p class="testi-box5_text">“ Sukses selalu untuk Bali Sanitya Global. Pelayanan dari proses
                                pengurusan dokumen sampai keberangkatan sangat baik.

                                ”</p>
                            <div class="testi-box5_wrapper">
                                <div class="testi-box5_profile">
                                    <div class="testi-box5_info">
                                        <h3 class="box-title">Ketut </h3>
                                    </div>
                                </div>
                                <div class="testi-quote"><img src="{{asset('assetsuser/img/icon/quote.svg')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-top-right overflow-hidden space-bottom" id="blog-sec" data-bg-src="assetsuser/img/bg/blog_bg_1.png">
    <div class="container space-bottom">
        <div class="title-area text-center"><span class="sub-title">
                <div class="icon-masking me-2"><span class="mask-icon"
                        data-mask-src="assetsuser/img/theme-img/title_shape_1.svg"></span> <img
                        src="assetsuser/img/theme-img/title_shape_1.svg" alt="shape"></div> BLOG
            </span>
            <h2 class="sec-title">Dapatkan <span class="text-theme">Berita</span> Terbaru Dari Kami</h2>
        </div>
        <div class="slider-area">
            <div class="swiper th-slider has-shadow" id="blogSlider1"
                data-slider-options='{"loop":true,"breakpoints":{"0":{"slidesPerView":1},"576":{"slidesPerView":"1"},"768":{"slidesPerView":"2"},"992":{"slidesPerView":"2"},"1200":{"slidesPerView":"3"}}}'>
                <div class="swiper-wrapper">
                    @foreach($bloguser as $user)
                    <div class="swiper-slide">
                        <div class="blog-card">
                            <div class="blog-img"><img src="{{ asset('images/blog/' . $user->photo) }}"
                                    alt="blog image"></div>
                            <div class="blog-content">
                                <div class="blog-meta"><a href="#"><i class="fal fa-calendar-days"></i>{{
                                        \Carbon\Carbon::parse($user->created_at)->isoFormat('DD MMMM YYYY') }}</a></div>
                                <h3 class="box-title"><a href="{{ route('blog.detail', ['slug' => $user->slug]) }}">{{
                                        $user->judul }}</a></h3>
                                <div class="blog-bottom"><a href="#" class="author"><img
                                            src="{{asset('assetsuser/images/logo.png')}}" width="30px" alt="avater"> By
                                        Admin</a> <a href="{{ route('blog.detail', ['slug' => $user->slug]) }}"
                                        class="line-btn">Read More<i class="fas fa-arrow-right"></i></a></div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div><button data-slider-prev="#blogSlider1" class="slider-arrow style3 slider-prev"><i
                    class="far fa-arrow-left"></i></button> <button data-slider-next="#blogSlider1"
                class="slider-arrow style3 slider-next"><i class="far fa-arrow-right"></i></button>
        </div>
    </div>
    <div class="container">
        <div class="slider-area text-center">
            <div class="swiper th-slider"
                data-slider-options='{"breakpoints":{"0":{"slidesPerView":2},"576":{"slidesPerView":"2"},"768":{"slidesPerView":"3"},"992":{"slidesPerView":"3"},"1200":{"slidesPerView":"4"},"1400":{"slidesPerView":"5"}}}'>
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 100px;">
                            <a href="/service"><img src="{{ asset('assetsuser/images/flag/australia.png') }}"
                                    alt="Australia"></a>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 100px;"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/inggris.png')}}" alt="Inggris"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 100px;"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/korea.png')}}" alt="Korea"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 100px;"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/jepang.png')}}" alt="Jepang"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 100px;"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/kuwait.png')}}" alt="Kuwait"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/malaysia.png')}}" alt="Malaysia">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/arabsaudi.png')}}" alt="Arab Saudia"></div>
                    </div>
                    <div class="swiper-slide">
                        <div class="brand-box" style="width: 240px; height: 110px;"><a href="/service"><img
                                    src="{{asset('assetsuser/images/flag/poland.png')}}" alt="Polandia"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shape-mockup" data-bottom="0" data-left="0">
        <div class="particle-2 small" id="particle-4"></div>
    </div>
</section>
@endsection

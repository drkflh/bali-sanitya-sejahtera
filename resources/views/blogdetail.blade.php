@extends('layouts.guest')

@section('title', "Blog Detail")

@section('content')
<div class="breadcumb-wrapper" style="position: relative; background-image: url('{{ asset('assetsuser/images/head_bg.jpg') }}'); background-size: cover; background-position: center; height: 100%;">
    <div class="overlay" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5); z-index: 1;"></div>
    <div class="breadcumb-content" style="position: relative; z-index: 2; color: white;">
        <h1 class="breadcumb-title" style="color: white;">Blog Detail</h1>
            <ul class="breadcumb-menu">
                <li><a href="/">Home</a></li>
                <li><a href="#">Blog Detail</a></li>
                <li>{{ $blog->judul }}</li>
            </ul>
        </div>
    </div>
</div>
<section class="th-blog-wrapper blog-details space-top space-extra-bottom">
    <div class="container">
        <div class="row">
            <div class="col-xxl-8 col-lg-7">
                <div class="th-blog blog-single">
                    <div class="blog-img"><img src="{{ asset('images/blog/' . $blog->photo) }}" alt="Blog Image"></div>
                    <div class="blog-content">
                        <div class="blog-meta"><a class="author" ><img
                                    src="{{asset('assetsuser/images/logo.png')}}" alt="avater"> By Admin</a>
                                    <i class="fa-light fa-calendar-days"></i> {{ \Carbon\Carbon::parse($blog->created_at)->isoFormat('DD MMMM YYYY') }}</a>
                        </div>
                        <h2 class="blog-title">{{ $blog->judul }}</h2>
                        <p>{!! html_entity_decode($blog->deskripsi ) !!} </p>
                    </div>
                </div>
            </div>
            <div class="col-xxl-4 col-lg-5">
                <aside class="sidebar-area">
                    <div class="widget widget_search">
                        <form class="search-form"><input type="text" placeholder="Enter Keyword"> <button
                                type="submit"><i class="far fa-search"></i></button></form>
                    </div>
                    <div class="widget widget_categories">
                        <h3 class="widget_title">Kategori</h3>
                        <ul>
                            @foreach ($categories as $category)
                                <li><a href="{{ route('blog.category', ['slug' => $category->slug_kategori]) }}">{{ $category->nama_kategori }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="widget">
                        <h3 class="widget_title">Postingan Galeru</h3>
                        <div class="sidebar-gallery">
                            @foreach($galleryImages as $image)
                            <div class="gallery-thumb">
                                <img src="{{ asset('images/gallery/' . $image->photo) }}" alt="Gallery Image">
                                <a href="{{ asset('images/gallery/' . $image->photo) }}" class="gallery-btn popup-image"><i class="fa fa-search"></i></a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="widget">
                        <h3 class="widget_title">Berita Terbaru</h3>
                        <div class="recent-post-wrap">
                            @foreach($recentPosts as $post)
                            <div class="recent-post">
                                <div class="media-img"><a href="{{ route('blog.detail', ['slug' => $post->slug]) }}"><img src="{{ asset('images/blog/' . $post->photo) }}" alt="Blog Image"></a></div>
                                <div class="media-body">
                                    <h4 class="post-title"><a class="text-inherit" href="{{ route('blog.detail', ['slug' => $post->slug]) }}">{{ $post->judul }}</a></h4>
                                    <div class="recent-post-meta"><a href="#"><i class="fal fa-calendar-days"></i>{{ $post->created_at->format('d F, Y') }}</a></div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</section>

@endsection

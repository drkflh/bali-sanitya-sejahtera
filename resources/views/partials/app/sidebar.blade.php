<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/dashboard">
        <div class="sidebar-brand-text mx-3">
            <img src="{{ asset('assetsuser/images/logo.png') }}" alt="Logo" height="85">
        </div>

    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item">
        <a class="nav-link" href="/dashboard">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Addons
    </div>

    <li class="nav-item">
        <a class="nav-link" href="/admin/kategori">
            <i class="fas fa-fw fa-list"></i>
            <span>Daftar Kategori</span></a>

        <a class="nav-link" href="/admin/blog">
            <i class="fas fa-fw fa-book"></i>
            <span>Data Blog</span></a>

        <a class="nav-link" href="/admin/gallery">
            <i class="fas fa-fw fa-images"></i>
            <span>Data Gallery</span></a>

        <a class="nav-link" href="/admin/contact">
            <i class="fas fa-fw fa-envelope"></i>
            <span>Data Kontak</span></a>

        <a class="nav-link" href="/admin/visitor">
            <i class="fas fa-fw fa-users"></i>
            <span>Data Visitor</span></a>
    </li>

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
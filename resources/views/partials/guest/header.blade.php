<div class="th-menu-wrapper">
    <div class="th-menu-area text-center"><button class="th-menu-toggle"><i class="fal fa-times"></i></button>
        <div class="mobile-logo"><a class="icon-masking" href="/"><span
                    data-mask-src="{{asset('assetsuser/images/logo.png')}}" class="mask-icon"></span><img
                    src="{{asset('assetsuser/images/logo.png')}}" style="width: 120px" alt="Bali Sanitya Sejahtera"></a>
        </div>
        <div class="th-mobile-menu">
            <ul>
                <li><a href="/">Beranda</a></li>
                <li><a href="/about">Tentang</a></li>
                <li><a href="/training">Pelatihan</a></li>
                <li><a href="/gallery">Galeri</a></li>
                <li><a href="/blog">Blog</a></li>
                <li><a href="/contact">Kontak</a></li>
            </ul>
        </div>
    </div>
</div>
<header class="th-header header-layout2">
    <div class="header-top">
        <div class="container">
            <div class="row justify-content-center justify-content-lg-between align-items-center gy-2">
                <div class="col-auto d-none d-lg-block">
                    <div class="header-links">
                        <ul>
                            <li><i class="fas fa-map-location"></i>Jl. Tukad Unda VIII No.8a, Panjer, Depasar Selatan, Kota Denpasar, Bali 80234</li>
                            <li><i class="fas fa-phone"></i><a href="https://wa.me/6282266489324" target="_blank">+62 822 6648 9324</a></li>
                            <li><i class="fas fa-envelope"></i><a
                                    href="mailto:balisanitya116@gmail.com">balisanitya116@gmail.com</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="header-social"><span class="social-title">Ikuti Kami : </span><a
                            href="https://www.instagram.com/bali_sanitya/"><i class="fab fa-instagram"></i></a> <a
                            href="https://wa.me/6282266489324" target="_blank"><i class="fab fa-whatsapp"></i></a>
                        <a href="https://www.youtube.com/channel/UCnCS3NqpGGpfs-CDWXWJJXQ"><i
                                class="fab fa-youtube"></i></a>
                        <a href="https://www.tiktok.com/discover/pt-bali-sanitya-global"><i class="fab fa-tiktok"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sticky-wrapper">
        <div class="menu-area">
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto">
                        <div class="header-logo"><a class="icon-masking" href="/"><span
                                    data-mask-src="{{asset('assetsuser/images/logo.png')}}" class="mask-icon"></span><img
                                    src="{{asset('assetsuser/images/logo.png')}}" style="width: 120px"
                                    alt="Bali Sanitya Sejahtera"></a></div>
                    </div>
                    <div class="col-auto">
                        <nav class="main-menu d-none d-lg-inline-block">
                            <ul>
                                <li><a href="/">Beranda</a></li>
                                <li><a href="/about">Tentang</a></li>
                                <li><a href="/training">Pelatihan</a></li>
                                <li><a href="/gallery">Galeri</a></li>
                                <li><a href="/blog">Blog</a></li>
                                <li><a href="/contact">Kontak</a></li>
                            </ul>
                        </nav>
                        <div class="header-button"><button type="button"
                                class="th-menu-toggle d-inline-block d-lg-none"><i class="far fa-bars"></i></button>
                        </div>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <div class="header-button">
                            <a href="https://forms.gle/VYk4DMJRfVH8dByv8" target="_blank" class="th-btn shadow-none">Daftar Disini<i class="fas fa-arrow-right ms-2"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

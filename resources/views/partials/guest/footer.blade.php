<footer class="footer-wrapper footer-layout2" data-bg-src="assetsuser/img/bg/footer_bg_1.jpg">
    <div class="widget-area">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-6 col-xxl-3 col-xl-4">
                    <div class="widget footer-widget">
                        <div class="th-widget-about">
                            <div class="about-logo"><a href="/"><img src="{{asset('assetsuser/images/logo.png')}}"
                                        style="width: 140px; height: 135px;" alt="Bali Sanitya Sejahtera"></a></div>
                            <p class="about-text">Bali Sanitya Sejahtera Foundation
                                Training Center & Sending Organization</p>
                            <div class="th-social">
                                <a href="https://www.instagram.com/bali_sanitya/">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="https://wa.me/6282266489324" target="_blank">
                                    <i class="fab fa-whatsapp"></i>
                                </a>
                                <a href="https://www.youtube.com/channel/UCnCS3NqpGGpfs-CDWXWJJXQ">
                                    <i class="fab fa-youtube"></i>
                                </a>
                                <a href="https://www.tiktok.com/discover/pt-bali-sanitya-global">
                                    <i class="fab fa-tiktok"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-auto">
                    <div class="widget widget_nav_menu footer-widget">
                        <h3 class="widget_title">Tautan Cepat</h3>
                        <div class="menu-all-pages-container">
                            <ul class="menu">
                                <li><a href="/">Beranda</a></li>
                                <li><a href="/about">Tentang</a></li>
                                <li><a href="/training">Pelatihan</a></li>
                                <li><a href="/gallery">Galeri</a></li>
                                <li><a href="/blog">Blog</a></li>
                                <li><a href="/contact">Kontak</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-auto">
                    <div class="widget footer-widget">
                        <h3 class="widget_title">Hubungi Kami</h3>
                        <div class="th-widget-contact">
                            <div class="contact-feature">
                                <div class="icon-btn"><i class="fa-solid fa-phone"></i></div>
                                <div class="media-body">
                                    <p class="contact-feature_label">Nomor Telepon</p><a
                                        href="https://wa.me/6282266489324" target="_blank"
                                        class="contact-feature_link">+62 822 6648 9324
                                    </a>
                                </div>
                            </div>
                            <div class="contact-feature">
                                <div class="icon-btn"><i class="fa-solid fa-envelope"></i></div>
                                <div class="media-body">
                                    <p class="contact-feature_label">Alamat Email</p><a
                                        href="mailto:balisanitya116@gmail.com"
                                        class="contact-feature_link">balisanitya116@gmail.com</a>
                                </div>
                            </div>
                            <div class="contact-feature">
                                <div class="icon-btn"><i class="fa-solid fa-location-dot"></i></div>
                                <div class="media-body">
                                    <p class="contact-feature_label">Lokasi Kantor</p><a
                                        href="https://www.google.com/maps" class="contact-feature_link"> Jl. Tukad Unda VIII No.8a, Panjer, Depasar Selatan, Kota Denpasar, Bali 80234</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3">
                    <div class="widget footer-widget">
                        <h3 class="widget_title">Bergabung Sekarang!!</h3>
                        <div class="newsletter-widget">
                            <p class="footer-text">Ayo Ajukan Jadwal Untuk Konsultasi Gratis
                            </p>
                            <a href="https://wa.me/6282266489324" target="_blank" class="th-btn shadow-none">Bergabung Sekarang<i
                                    class="fas fa-arrow-right ms-2"></i></a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="copyright-wrap bg-theme">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-lg-10">
                    <p class="copyright-text">Copyright <i class="fal fa-copyright"></i> 2024 <a href="/">Designed and
                            Developed by IT BALI SANITYA SEJAHTERA</a>. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
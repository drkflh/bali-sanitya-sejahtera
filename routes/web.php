<?php

use App\Http\Controllers\ContactController;
use App\Http\Controllers\DashboardAdminController;
use App\Http\Controllers\UserDashboardController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\VisitorController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\BlogController;
use Illuminate\Support\Facades\Artisan;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/optimize', function() {
    Artisan::call('optimize');
});


Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Route::get('/training', function () {
    return view('services');
});

Route::get('/training/japanese-language-training', function () {
    return view('japaneselanguagetraining');
});

Route::get('/training/spa-training', function () {
    return view('spatraining');
});

Route::get('/training/korea-language-training', function () {
    return view('korealanguagetraining');
});

Route::get('/training/inggris-language-training', function () {
    return view('inggrislanguagetraining');
});

Route::get('/error', function () {
    return view('errors.404');
});

Route::get('/', [UserDashboardController::class, 'Index'])->name('index');
Route::get('/about', [UserDashboardController::class, 'AboutIndex'])->name('about.index');
Route::get('/blog', [UserDashboardController::class, 'BlogIndex'])->name('blog.user');
Route::get('/blog/category/{slug}', [UserDashboardController::class, 'blogCategory'])->name('blog.category');
Route::get('/blog/{slug}', [UserDashboardController::class, 'BlogDetail'])->name('blog.detail');
Route::get('/gallery', [UserDashboardController::class, 'GalleryIndex'])->name('gallery.user');
Route::post('/contact/store', [ContactController::class, 'store'])->name('contact.store');

Route::get('/dashboard', [DashboardAdminController::class, 'index'])->middleware(['auth', 'verified'])->name('dashboard');


Route::middleware('auth')->group(function () {


    // GALLERY ADMIN
    Route::get('/admin/gallery', [GalleryController::class, 'index'])->name('galleries.index');
    Route::post('/admin/gallery', [GalleryController::class, 'store'])->name('galleries.store');
    Route::delete('admin/{gallery}', [GalleryController::class, 'destroy'])->name('galleries.destroy');
    Route::put('admin/gallery/{gallery}', [GalleryController::class, 'update'])->name('galleries.update');
    Route::patch('admin/{gallery}/toggle', [GalleryController::class, 'toggle'])->name('galleries.toggle');

    // BLOG ADMIN
    Route::get('/admin/blog', [BlogController::class, 'index'])->name('blog.index');
    Route::post('/admin/blog', [BlogController::class, 'store'])->name('blog.store');
    Route::put('admin/blog/{blog}', [BlogController::class, 'update'])->name('blog.update');
    Route::delete('admin/blog/{blog}', [BlogController::class, 'destroy'])->name('blog.destroy');
    Route::patch('admin/blog/{blog}/toggle', [BlogController::class, 'toggle'])->name('blog.toggle');

    // KATEGORI ADMIN
    Route::get('/admin/kategori', [CategoryController::class, 'index'])->name('category.index');
    Route::post('/admin/kategori', [CategoryController::class, 'store'])->name('category.store');
    Route::delete('admin/kategori/{id}', [CategoryController::class, 'destroy'])->name('category.destroy');
    Route::put('/admin/kategori/{category}', [CategoryController::class, 'update'])->name('category.update');


    // CONTACT ADMIN
    Route::get('/admin/contact', [ContactController::class, 'index'])->name('contact.index');

    //VISITOR
    Route::get('/admin/visitor', [VisitorController::class, 'visitor'])->name('visitor.page');
});

require __DIR__.'/auth.php';

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    use HasFactory;
    protected $fillable = [
        'ip_address',
        'visited_at',
        'browser',
        'platform',
        'device_type',
    ];

    protected $dates = ['visited_at'];
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecordVisitorMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $visitorData = [
            'ip_address' => $request->ip(),
            'visited_at' => now(),
            'browser' => $this->getBrowser($request->header('User-Agent')),
            'platform' => $this->getPlatform($request->header('User-Agent')),
            'device_type' => $this->getDeviceType($request->header('User-Agent')),
        ];

        DB::table('visitors')->insert($visitorData);

        return $next($request);
    }

    private function getBrowser($userAgent)
    {
        $userAgent = strtolower($userAgent);
    
        if (strpos($userAgent, 'chrome') !== false) {
            return 'Chrome';
        } elseif (strpos($userAgent, 'firefox') !== false) {
            return 'Mozilla Firefox';
        } elseif (strpos($userAgent, 'opera mini') !== false || strpos($userAgent, 'opera') !== false) {
            return 'Opera';
        } elseif (strpos($userAgent, 'safari') !== false) {
            return 'Safari';
        } elseif (strpos($userAgent, 'edge') !== false) {
            return 'Microsoft Edge';
        } elseif (strpos($userAgent, 'msie') !== false || strpos($userAgent, 'trident') !== false) {
            return 'Internet Explorer';
        } else {
            return 'Other Browser';
        }
    }

    private function getPlatform($userAgent)
    {
        $userAgent = strtolower($userAgent);
        
        if (strpos($userAgent, 'windows') !== false) {
            return 'Windows';
        } elseif (strpos($userAgent, 'macintosh') !== false) {
            return 'Mac';
        } elseif (strpos($userAgent, 'linux') !== false) {
            return 'Linux';
        } elseif (strpos($userAgent, 'android') !== false) {
            return 'Android';
        } elseif (strpos($userAgent, 'iphone') !== false || strpos($userAgent, 'ipad') !== false) {
            return 'iOS';
        } else {
            return 'Other';
        }
    }

    private function getDeviceType($userAgent)
    {
        $userAgent = strtolower($userAgent);
        
        if (strpos($userAgent, 'mobile') !== false || strpos($userAgent, 'android') !== false || strpos($userAgent, 'iphone') !== false || strpos($userAgent, 'ipad') !== false) {
            return 'Mobile';
        } elseif (strpos($userAgent, 'windows') !== false || strpos($userAgent, 'macintosh') !== false || strpos($userAgent, 'linux') !== false) {
            return 'Desktop';
        } else {
            return 'Other';
        }
    }
}

<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Gallery;
use App\Models\Contact;
use App\Models\Visitor;
use Carbon\Carbon;

class DashboardAdminController extends Controller
{
    public function index()
    {
        $countblog = Blog::all()->count();
        $countimage = Gallery::all()->count();
        $countcontact = Contact::all()->count();

        $visitors = Visitor::where('visited_at', '>=', Carbon::now()->subDays(7))
                            ->orderBy('visited_at', 'asc')
                            ->get()
                            ->groupBy(function($date) {
                                return Carbon::parse($date->visited_at)->format('Y-m-d');
                            });

        $visitorData = [];
        $dates = [];
        $counts = [];

        foreach ($visitors as $date => $visitorGroup) {
            $dates[] = $date;
            $counts[] = $visitorGroup->count();
        }

        return view('dashboard', compact('countblog', 'countimage', 'countcontact', 'dates', 'counts'));
    }
}

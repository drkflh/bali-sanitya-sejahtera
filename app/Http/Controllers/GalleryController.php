<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{

    public function index()
    {
        $galleries = Gallery::all();
        return view('admin.galery', compact('galleries'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        $imageName = time().'.'.$request->photo->extension();  
    
        $request->photo->move(public_path('images/gallery'), $imageName);
    
        Gallery::create(['photo' => $imageName]);
    
        return redirect()->route('galleries.index')
                        ->with('success','Image added successfully.');
    }
    
    
    public function update(Request $request, Gallery $gallery)
    {
        $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        if ($request->hasFile('photo')) {
            if ($gallery->photo) {
                $oldImagePath = public_path('images/gallery/' . $gallery->photo);
                if (file_exists($oldImagePath)) {
                    unlink($oldImagePath);
                }
            }
    
            $imageName = time().'.'.$request->photo->extension();
            $request->photo->move(public_path('images/gallery'), $imageName);
            $gallery->photo = $imageName;
        }
    
        $gallery->save();
    
        return redirect()->route('galleries.index')
                        ->with('success', 'Photo updated successfully');
    }
    


    public function destroy(Gallery $gallery)
    {
        $imagePath = public_path('images/gallery/' . $gallery->photo);
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }
    
        $gallery->delete();
    
        return redirect()->route('galleries.index')
                         ->with('success', 'Photo deleted successfully');
    }
    
    public function toggle(Gallery $gallery)
    {
        $gallery->is_active = !$gallery->is_active;
        $gallery->save();

        return redirect()->route('galleries.index')->with('success', 'Image status updated successfully.');
    }
}

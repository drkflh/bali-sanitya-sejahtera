<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class VisitorController extends Controller
{
    public function visitor()
    {
        $visitors = DB::table('visitors')->latest()->paginate(30);

        return view('admin.visitors', compact('visitors'));
    }
}

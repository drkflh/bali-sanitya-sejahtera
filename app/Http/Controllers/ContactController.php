<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public function index()
    {
        $contact = Contact::all();
        return view('admin.contact', compact('contact'));
    }

 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'namaawalan' => 'required|string',
            'namaakhiran' => 'required|string',
            'email' => 'required|email',
            'nomorhp' => 'required|string',
            'pesan' => 'required|string',
        ]);
    
        Contact::create($validatedData);
    
        return view('contact')->with('success', 'Message added successfully.');
    }
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Gallery;


class UserDashboardController extends Controller
{

     public function blogIndex(Request $request)
    {
        $categories = Category::all();
        $searchTerm = $request->input('query');
        $query = Blog::where('is_active', true);

        if ($searchTerm) {
            $query->where('judul', 'LIKE', '%' . $searchTerm . '%');
        }

        $bloguser = $query->orderBy('created_at', 'desc')->get();
        $recentPosts = Blog::where('is_active', true)
            ->orderBy('created_at', 'desc')
            ->take(3)
            ->get();
        $galleryImages = Gallery::latest()->take(6)->get();

        return view('blog', compact('bloguser', 'galleryImages', 'recentPosts', 'categories'));
    }

    public function blogCategory($slug)
    {
        $categories = Category::all();
        $category = Category::where('slug_kategori', $slug)->firstOrFail();
        $blogs = $category->blogs()->where('is_active', true)->orderBy('created_at', 'desc')->get();
        $recentPosts = Blog::where('is_active', true)
            ->orderBy('created_at', 'desc')
            ->take(3)
            ->get();
        $galleryImages = Gallery::latest()->take(6)->get();

        return view('blog', compact('category', 'blogs', 'galleryImages', 'recentPosts', 'categories'));
    }
    public function blogDetail($slug)
    {
        $categories = Category::all();
        $recentPosts = Blog::where('is_active', true)
            ->orderBy('created_at', 'desc')
            ->take(3)
            ->get();
        
        $blog = Blog::where('slug', $slug)->firstOrFail();
        $galleryImages = Gallery::latest()->take(6)->get();

        return view('blogdetail', compact('blog', 'galleryImages', 'recentPosts', 'categories'));
    }

    public function GalleryIndex()
    {
        $galleryuser = Gallery::where('is_active', true)->get();
        return view('gallery', compact('galleryuser'));
    }

    public function Index()
    {
        $bloguser = Blog::where('is_active', true)->take(6)->get();
        return view('welcome', compact('bloguser'));
    }
    public function AboutIndex()
    {
        $bloguser = Blog::where('is_active', true)->take(6)->get();
        return view('about', compact('bloguser'));
    }

}

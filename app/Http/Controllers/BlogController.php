<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BlogController extends Controller
{

    public function index()
    {
        $blogadmin = Blog::with('category')->get(); 
        $categories = Category::all();

        return view('admin.blog', compact('blogadmin', 'categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|string|max:255',
            'deskripsi' => 'required|string',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'category_id' => 'required|exists:categories,id', 
        ]);

        $imageName = time().'.'.$request->photo->extension();  
        $request->photo->move(public_path('images/blog'), $imageName);

        $blog = Blog::create([
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'photo' => $imageName,
            'slug' => Str::slug($request->judul),
            'category_id' => $request->category_id, 
        ]);

        return redirect()->route('blog.index')
                         ->with('success', 'Blog added successfully.');
    }


    public function update(Request $request, Blog $blog)
    {
        $request->validate([
            'judul' => 'required|string|max:255',
            'deskripsi' => 'required|string',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'category_id' => 'required|exists:categories,id', 
        ]);

        $blog->judul = $request->judul;
        $blog->deskripsi = $request->deskripsi;

        if ($request->hasFile('photo')) {
            $imagePath = public_path('images/blog/' . $blog->photo);
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }

            $imageName = time().'.'.$request->photo->extension();
            $request->photo->move(public_path('images/blog'), $imageName);
            $blog->photo = $imageName;
        }

        $blog->slug = Str::slug($request->judul);
        $blog->category_id = $request->category_id;

        $blog->save();

        return redirect()->route('blog.index')
                         ->with('success', 'Blog updated successfully');
    }

    public function destroy(Blog $blog)
    {
        $imagePath = public_path('images/blog/' . $blog->photo);
        if (file_exists($imagePath)) {
            unlink($imagePath);
        }
    
        $blog->delete();
    
        return redirect()->route('blog.index')
                         ->with('success', 'Blog deleted successfully');
    }
    
    public function toggle(Blog $blog)
    {
        $blog->is_active = !$blog->is_active;
        $blog->save();

        return redirect()->route('blog.index')->with('success', 'Blog status updated successfully.');
    }
}

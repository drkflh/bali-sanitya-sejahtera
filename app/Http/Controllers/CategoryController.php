<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories', ['categories' => $categories]);
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'nama_kategori' => 'required|unique:categories'
        ]);

        Category::create([
            'nama_kategori' => $request->nama_kategori,
            'slug_kategori' => Str::slug($request->nama_kategori)
        ]);

        return redirect()->route('category.index');
    }

    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
    
        $request->validate([
            'nama_kategori' => 'required|unique:categories,nama_kategori,' . $category->id,
        ]);
    
        $category->nama_kategori = $request->nama_kategori;
        $category->slug_kategori = Str::slug($request->nama_kategori, '-');
    
        $category->save();
    
        return redirect()->route('category.index')->with('success', 'Category updated successfully');
    }
    

    public function destroy($id)
    {
        $category = Category::find($id);
    
        if (!$category) {
            return redirect()->route('category.index')->with('error', 'Category not found.');
        }
    
        $category->delete();
        return redirect()->route('category.index')->with('success', 'Category deleted successfully.');
    }
    
}
